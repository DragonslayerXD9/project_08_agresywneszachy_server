package com.demo.springboot.service;

import com.demo.springboot.domain.dto.FileData;
import com.demo.springboot.domain.dto.PizzaData;
import com.demo.springboot.domain.dto.UserDataDto;

import java.io.IOException;
import java.util.List;

public interface FileService {

    List<PizzaData> findAll(String path) throws IOException;

    FileData createFile(UserDataDto userDataDto, String path);

}
