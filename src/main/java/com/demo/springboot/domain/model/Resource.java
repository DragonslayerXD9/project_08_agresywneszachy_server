package com.demo.springboot.domain.model;

import com.demo.springboot.domain.dto.FileData;
import com.demo.springboot.domain.dto.PizzaData;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface Resource {
    String fileName = "pizza.csv";

    void saveOne(FileData FileData, String path);

    List<PizzaData> findAll() throws IOException;
}
