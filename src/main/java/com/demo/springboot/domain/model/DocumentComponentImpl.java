package com.demo.springboot.domain.model;

import com.demo.springboot.domain.dto.UserDataDto;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.beans.XMLEncoder;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;

@Component
public class DocumentComponentImpl implements DocumentComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentComponentImpl.class);

    @Override
    public void createDocument(UserDataDto userDataDto, String fileDestination) {
        try {
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileDestination));
            document.open();
            PdfPTable table = new PdfPTable(6);
            table.setWidths(new int[]{2, 2, 2, 2, 2, 2});
            table.addCell(createCell("Imię i nazwisko", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Ulica", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Miasto", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Produkty", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Cena", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Zniżka", 2, Element.ALIGN_LEFT));
            String serializedMap = userDataDto.getOrderList().toString().replace("}","");
            serializedMap = serializedMap.replace("{","");
            serializedMap = serializedMap.replace("="," - ");


            table.addCell(createCell(userDataDto.getName(), 1, Element.ALIGN_LEFT));
            table.addCell(createCell(userDataDto.getStreet(), 1, Element.ALIGN_LEFT));
            table.addCell(createCell(userDataDto.getCity()+" "+ userDataDto.getPostcode(), 1, Element.ALIGN_LEFT));
            table.addCell(createCell(serializedMap, 1, Element.ALIGN_LEFT));
            table.addCell(createCell(String.valueOf(userDataDto.getTotalPrice()), 1, Element.ALIGN_LEFT));
            table.addCell(createCell(String.valueOf(userDataDto.getDiscount()), 1, Element.ALIGN_LEFT));
            document.add(table);


            document.close();

        } catch (DocumentException | FileNotFoundException e) {
            LOGGER.error("i can't create document or file not exists");
        }
    }

    private PdfPCell createCell(String content, float borderWidth, int alignment) {
        final String FONT = "static/arial.ttf";

        Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, true);

        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setBorderWidth(borderWidth);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingTop(3);
        cell.setPaddingBottom(6);
        cell.setPaddingLeft(3);
        cell.setPaddingRight(3);
        return cell;
    }

    private void setBackgroundAsGradient(Document document, PdfWriter writer) {
        Rectangle pageSize = document.getPageSize();
        PdfShading axial = PdfShading.simpleAxial(writer,
                pageSize.getLeft(pageSize.getWidth()/10), pageSize.getBottom(),
                pageSize.getRight(pageSize.getWidth()/10), pageSize.getBottom(),
                new BaseColor(255, 255, 255),
                new BaseColor(255, 255, 255), true, true);
        PdfContentByte canvas = writer.getDirectContentUnder();
        canvas.paintShading(axial);
    }
}
