package com.demo.springboot.domain.model;

import com.demo.springboot.domain.dto.FileData;
import com.demo.springboot.domain.dto.PizzaData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ResourceDAO implements Resource {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceDAO.class);

    @Override
    public List<PizzaData> findAll() throws IOException {
        List<PizzaData> files = new ArrayList<>();


        try {
            final List<String> lines = Files.readAllLines(Paths.get("src/main/resources/data/pizza.csv"), StandardCharsets.UTF_8);

            for (final String line : lines) {
                PizzaData PizzaData = convertLineToData(line.split(","));
                files.add(PizzaData);
            }

        } catch (IOException e) {
            LOGGER.error("file {} not found", Resource.fileName);
        }

        return files;
    }

    private PizzaData convertLineToData(final String[] line) {

        String Name = line[0];
        double price = Double.parseDouble(line[1]);


        return new PizzaData(Name, price);
    }

    @Override
    public void saveOne(FileData fileData, String path) {
        String fileUrl = path + Resource.fileName;

        try {
            PrintWriter pw = new PrintWriter(new FileOutputStream(new File(fileUrl), true));
            pw.write(fileData.getFileName() + "," + fileData.getSize() + "," + fileData.getCreationDate() + "\n");
            pw.close();

        } catch (FileNotFoundException e) {
            LOGGER.error("file {} not found", Resource.fileName);
        }
    }
}
