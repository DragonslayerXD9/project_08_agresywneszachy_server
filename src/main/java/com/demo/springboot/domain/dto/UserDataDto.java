package com.demo.springboot.domain.dto;

import java.io.Serializable;
import java.util.HashMap;

public class UserDataDto {
        private String name;
        private String street;
        private String city;
        private String postcode;
        private HashMap<String, Integer> orderList;
        private double discount;
        private double totalPrice;

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getPostcode() {
        return postcode;
    }

    public HashMap<String, Integer> getOrderList() {
        return orderList;
    }

    public void setOrderList(HashMap<String, Integer> orderList) {
        this.orderList = orderList;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public UserDataDto() {
    }

        @Override
        public String toString() {
            return "UserDataDto{" +
                    "firstName='" + name + '\'' +
                    ", description='" + name + '\'' +
                    '}';
        }
}
